import React from 'react';
import {Box, Flex} from '@rebass/grid'
import { Form } from 'react-bootstrap';

import {TITLES, STATE} from './cms';

class ContactInfo extends React.Component{

    render(){
        const { emergencyContact } = this.props;
        return(
            <Form>
                <Flex flexDirection={["column", "row", "row"]}>
                    <Box width={["100%", '15%', "15%"]} px={2}>
                        <Form.Label for="title">Title</Form.Label>
                        <Form.Control as="select" id="title">
                            {TITLES.map(({value, label}) => (<option value={value}>{label}</option>))}
                        </Form.Control>
                    </Box>
                    <Box width={["100%", '40%', "40%"]} px={2}>
                        <Form.Label for="firstName">First Name</Form.Label>
                        <Form.Control type="text" id="firstName" placeholder="First name"/>
                    </Box>
                    <Box width={["100%", "40%", "40%"]} px={2}>
                        <Form.Label for="lastName">Last Name</Form.Label>
                        <Form.Control type="text" id="lastName" placeholder="Last name"/>
                    </Box>
                </Flex>

                <Box width={["100%", "50%", "50%"]} px={2}>
                    <Form.Label for="emailAddress">Email Address</Form.Label>
                    <Form.Control type="email" id="emailAddress"/>
                </Box>

                <Box width={["100%", "50%", "50%"]} px={2}>
                    <Form.Label for="phone">Contact Number</Form.Label>
                    <Form.Control type="phone" id="phone"/>
                </Box>
                {!emergencyContact && (
                    <Box width={["100%", "50%", "50%"]} px={2}>
                        <Form.Label for="dob">Date of Birth</Form.Label>
                        <Form.Control type="date" id="dob"/>
                    </Box>
                )}

                <Box width={["100%", "50%", "50%"]} px={2}>
                    <Form.Label for="address1">Address Line1</Form.Label>
                    <Form.Control type="text" id="address1"/>
                </Box>

                <Box width={["100%", "50%", "50%"]} px={2}>
                    <Form.Label for="address2">Address Line2</Form.Label>
                    <Form.Control type="text" id="address2"/>
                </Box>

                <Flex flexDirection={["column", "row", "row"]}>
                    <Box width={["100%", '40%', "40%"]} px={2}>
                        <Form.Label for="city">City/Suburb</Form.Label>
                        <Form.Control type="text" id="city" placeholder="Sydney"/>
                    </Box>
                    <Box width={["100%", '15%', "15%"]} px={2}>
                        <Form.Label for="title">State</Form.Label>
                        <Form.Control as="select" id="state">
                            {STATE.map(({value, label}) => (<option value={value}>{label}</option>))}
                        </Form.Control>
                    </Box>
                    <Box width={["100%", "40%", "40%"]} px={2}>
                        <Form.Label for="postcode">Postcode</Form.Label>
                        <Form.Control type="text" id="postcode" placeholder="2000"/>
                    </Box>
                </Flex>

            </Form>
        );
    }
}
export default ContactInfo;