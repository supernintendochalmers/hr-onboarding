import React from 'react';
import { Card, Navbar } from 'react-bootstrap';
import { Flex, Box} from '@rebass/grid';
import ContactInfo from './ContactInfo';
import PayrollInfo from './PayrollInfo';


class Main extends React.Component{
    render(){
        return(
            <React.Fragment>
                <main className="main main-padded">
                    <Navbar bg="dark" variant="dark"><Navbar.Brand> HR Onboarding Form</Navbar.Brand></Navbar>
                    <div className="container-fluid">
                        <Box padding="20px" width="100%"> 
                            <h4>Personal Information</h4>
                            <ContactInfo/>
                        </Box>
                        <Box padding="20px" width="100%"> 
                            <h5>Payroll Infomation</h5>
                            <PayrollInfo/>
                        </Box>
                        <Box padding="20px" width="100%"> 
                            <h4>Emergency Contact</h4>
                            <ContactInfo emergencyContact/>
                        </Box>
                    </div>
                </main>
            </React.Fragment>
        );
    }
}
export default Main;