export const TITLES = [
    {label: "Mr.", value: "MR"},
    {label: "Mrs.", value: "MRS"},
    {label: "Ms.", value: "MS"},
    {label: "Dr.", value: "DR"},
    {label: "Prof.", value: "PROF"}
];

export const STATE = [
    {label: "NSW", value: "NSW"},
    {label: "VIC", value: "VIC"},
    {label: "QLD", value: "QLD"},
    {label: "SA", value: "SA"},
    {label: "WA", value: "WA"},
    {label: "TAS", value: "TAS"},
    {label: "NT", value: "NT"},
    {label: "ACT", value: "ACT"}
];