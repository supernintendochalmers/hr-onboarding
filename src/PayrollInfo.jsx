import React from 'react';
import {Box, Flex} from '@rebass/grid'
import { Form } from 'react-bootstrap';

import {TITLES, STATE} from './cms';

class PayrollInfo extends React.Component{

    render(){
        return(
            <Form>
                <Flex flexDirection={["column", "row", "row"]}>
                    <Box width={["100%", '40%', "40%"]} px={2}>
                        <Form.Label for="tfn">Tax File Number</Form.Label>
                        <Form.Control type="text" id="tfn" placeholder="TFN"/>
                    </Box>
                    <Box width={["100%", "40%", "40%"]} px={2}>
                        <Form.Label for="txfree">Claim Tax Free threshold</Form.Label>
                        <Form.Check id="txfree"/>
                    </Box>
                </Flex>

                <Box width={["100%", "50%", "50%"]} px={2}>
                    <Form.Label for="bank">Banking Institution</Form.Label>
                    <Form.Control type="text" id="bank"/>
                </Box>

                <Box width={["100%", "50%", "50%"]} px={2}>
                    <Form.Label for="bankAccountName">Account name</Form.Label>
                    <Form.Control type="text" id="bankAccountName"/>
                </Box>
                
                <Box width={["100%", "50%", "50%"]} px={2}>
                    <Form.Label for="branch">Branch</Form.Label>
                    <Form.Control type="text" id="branch"/>
                </Box>

                <Flex flexDirection={["column", "row", "row"]}>
                    <Box width={["100%", '40%', "40%"]} px={2}>
                        <Form.Label for="bsb">BSB</Form.Label>
                        <Form.Control type="text" id="bsb" placeholder="XXXXXX"/>
                    </Box>
                    <Box width={["100%", "40%", "40%"]} px={2}>
                        <Form.Label for="accountNumber">Account Number</Form.Label>
                        <Form.Control type="text" id="accountNumber"/>
                    </Box>
                </Flex>

            </Form>
        );
    }
}
export default PayrollInfo;